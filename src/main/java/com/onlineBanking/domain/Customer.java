package com.onlineBanking.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;




@Entity
public class Customer {
	@Id
	@GeneratedValue(strategy =  GenerationType.AUTO)
	@Column(updatable = false)
	private int id;
	
	@NotNull
    @Size(max = 10)
	@Column(unique = true)
	private String username;
	
	@NotNull
    @Size(max = 10)
	private String password;
	
	@NotNull
	@Column(name = "email", unique = true)
	private String email;
	
	@NotNull
    @Size(max = 10)
	private String mobileNo;
	
	@NotNull
    @Size(max = 40)
	private String firstName;
	
	@NotNull
    @Size(max = 40)
	private String lastName;
	
	@NotNull
    @Size(max = 200)
	private String address;
	
	@NotNull
    @Size(max = 30)
	private String city;
	
	@NotNull
    @Size(max = 10)
	private String zip;
	
	@NotNull
    @Size(max = 2)
	private String state;
	
	/*
	 * @OneToMany(mappedBy = "customer", cascade = CascadeType.PERSIST, fetch =
	 * FetchType.LAZY)
	 * 
	 * @JsonIgnore private List<Account> accountList;
	 */
	
	
	@Override
	public String toString() {
		return "Customer [id=" + id + ", username=" + username + ", password=" + password + ", email=" + email
				+ ", mobileNo=" + mobileNo + ", firstName=" + firstName + ", lastName=" + lastName + ", address="
				+ address + ", city=" + city + ", zip=" + zip + ", state=" + state + "]";
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}
	/**
	 * @param zip the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	
}

