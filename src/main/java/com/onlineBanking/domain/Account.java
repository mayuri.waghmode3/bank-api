package com.onlineBanking.domain;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

@Entity
public class Account {
	
    @Size(max = 8)
	@Id
	private String accNo;
    
    private BigDecimal balance;
    
    @ManyToOne
	@JoinColumn(name = "id")
	private Customer customer;
    
	@ManyToOne
	@JoinColumn(name = "type_id")
	private AccountType accountType;

	@Override
	public String toString() {
		return "Account [accNo=" + accNo + ", balance=" + balance + ", customer=" + customer + ", accountType="
				+ accountType + "]";
	}

	public String getAccNo() {
		return accNo;
	}

	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}

	
	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}
			
			
			
}
