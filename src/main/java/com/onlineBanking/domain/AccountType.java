package com.onlineBanking.domain;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
public class AccountType {
	@Id
	@Size(max = 3)
	private String typeId;
			
	@NotNull
	private String type;
			
	private String description;
			
	private float interest_rate;
			
	/*
	 * @OneToMany(mappedBy = "accountType", cascade = CascadeType.ALL, fetch =
	 * FetchType.LAZY)
	 * 
	 * @JsonIgnore private List<Account> accountList;
	 */
	

	

	public String getTypeId() {
		return typeId;
	}

	@Override
	public String toString() {
		return "AccountType [typeId=" + typeId + ", type=" + type + ", description=" + description + ", interest_rate="
				+ interest_rate + "]";
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getInterest_rate() {
		return interest_rate;
	}

	public void setInterest_rate(float interest_rate) {
		this.interest_rate = interest_rate;
	}

}
