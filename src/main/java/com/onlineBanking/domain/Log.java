package com.onlineBanking.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Log {
	@Id
	@GeneratedValue(strategy =  GenerationType.AUTO)
	@Column(updatable = false)
    private int logId;
    private Date timestamp;

    private String operation;

    @ManyToOne
    @JoinColumn(name = "id")
    private Customer customer;
    


	@Override
	public String toString() {
		return "Log [logId=" + logId + ", timestamp=" + timestamp + ", operation=" + operation + ", customer="
				+ customer + "]";
	}


	public long getLogId() {
		return logId;
	}

	
	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public void setLogId(int logId) {
		this.logId = logId;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

    
}
