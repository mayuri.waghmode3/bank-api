package com.onlineBanking.controller;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.onlineBanking.domain.Account;
import com.onlineBanking.domain.Customer;
import com.onlineBanking.domain.Log;
import com.onlineBanking.service.AccountService;
import com.onlineBanking.service.CustomerService;
import com.onlineBanking.service.LogService;
import com.onlineBanking.validators.PaymentValidator;



@CrossOrigin("*")
@RestController
public class PaymentController {
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private LogService logService;
	
	private PaymentValidator validator;
	
	JSONObject json = new JSONObject();
	
	//get balance
	@RequestMapping(value = "/getBalance/{accNo}", method = RequestMethod.GET)
	public ResponseEntity<?> getBalance(@PathVariable String accNo){
		Account account = accountService.findByAccNo(accNo);
		validator = new PaymentValidator();
		json.put("code", 200);
		json.put("message", "Your current balance is $"+account.getBalance());
		return ResponseEntity.ok().body(json);
	}
	
	//withdraw amount
	@RequestMapping(value = "/withdraw", method = RequestMethod.PUT)
	public ResponseEntity<?> withdraw(@RequestParam BigDecimal amount, @RequestParam String accNo){
		Account account = accountService.findByAccNo(accNo);
		validator = new PaymentValidator();
        if(validator.validate(account,amount)) {
        	account.setBalance(account.getBalance().subtract(amount));
            accNo = accountService.saveAccount(account);
            
            Log log = new Log();
            log.setOperation("Withdraw: From Account No " + account.getAccNo()  + " Amount $" + amount);
            log.setTimestamp(new Timestamp(System.currentTimeMillis()));
            
            String username = account.getCustomer().getUsername();
            Customer customer = customerService.findByUsername(username);
            log.setCustomer(customer);

            logService.save(log);
            json.put("code", 200);
    		json.put("message", "WithDrawn Successfully. Your current balance is $"+account.getBalance());
    		return ResponseEntity.ok().body(json);
            
        }
	    else {
	        json.put("code", 400);
			json.put("message", "Insufficient balance");
			return ResponseEntity.ok().body(json);
	    }
	}
		
		
	//Deposit amount
	@RequestMapping(value = "/deposit", method = RequestMethod.PUT)
	public ResponseEntity<?> deposit(@RequestParam BigDecimal amount, @RequestParam String accNo){
		Account account = accountService.findByAccNo(accNo);
		validator = new PaymentValidator();
	       if(validator.validate(amount)) {
	            account.setBalance(account.getBalance().add(amount));
	            accNo = accountService.saveAccount(account);
	            
	            Log log = new Log();
	            log.setOperation("Deposit: To Account No " + account.getAccNo()  + " Amount $" + amount);
	            log.setTimestamp(new Timestamp(System.currentTimeMillis()));
	            
	            String username = account.getCustomer().getUsername();
	            Customer customer = customerService.findByUsername(username);
	            log.setCustomer(customer);

	            logService.save(log);
	            json.put("code", 200);
				json.put("message", "Deposited successfully. Your current balance is $"+account.getBalance());
				return ResponseEntity.ok().body(json);
	        }
		    else {
		    	json.put("code", 400);
				json.put("message", "Invalid amount");
				return ResponseEntity.ok().body(json);
		    }
		}
	
	//Transfer 
	@RequestMapping(value = "/transfer", method = RequestMethod.PUT)
	public ResponseEntity<?> transfer(@RequestParam BigDecimal amount, @RequestParam String fromAccNo,@RequestParam String toAccNo) {
		Account accountFrom = accountService.findByAccNo(fromAccNo);
		Account accountTo = accountService.findByAccNo(toAccNo);
	    validator = new PaymentValidator();
	        if(validator.validate(accountFrom,amount)) {
	        	accountFrom.setBalance(accountFrom.getBalance().subtract(amount));
	        	fromAccNo = accountService.saveAccount(accountFrom);
	        	
	        	accountTo.setBalance(accountTo.getBalance().add(amount));
	        	toAccNo = accountService.saveAccount(accountTo);
	        	
	        	Log log = new Log();
	            log.setOperation("Transaction: From Account No: " + accountFrom.getAccNo() + " To Account No: " + accountTo.getAccNo() + " Amount $" + amount);
	            log.setTimestamp(new Timestamp(System.currentTimeMillis()));
	            
	            String username = accountFrom.getCustomer().getUsername();
	            log.setCustomer(customerService.findByUsername(username));
	            logService.save(log);
	            username = accountTo.getCustomer().getUsername();
	            log.setCustomer(customerService.findByUsername(username));
	            logService.save(log);
	           
	            json.put("code", 200);
				json.put("message", "Transfered successfully. Your current balance is $"+accountFrom.getBalance());
				return ResponseEntity.ok().body(json);
	        }
	        else {
	        	json.put("code", 400);
				json.put("message", "Transaction aborted.");
				return ResponseEntity.ok().body(json);
	        	}
	        
	}
	
		
}
