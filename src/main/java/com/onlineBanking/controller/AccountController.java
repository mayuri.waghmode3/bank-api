package com.onlineBanking.controller;

import java.sql.Timestamp;
import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.onlineBanking.domain.Account;
import com.onlineBanking.domain.AccountType;
import com.onlineBanking.domain.Customer;
import com.onlineBanking.domain.Log;
import com.onlineBanking.service.AccountService;
import com.onlineBanking.service.AccountTypeService;
import com.onlineBanking.service.CustomerService;
import com.onlineBanking.service.LogService;
@CrossOrigin("*")
@RestController
public class AccountController {
	@Autowired
	 private AccountService accountService;
	@Autowired
	private AccountTypeService accountTypeService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private LogService logService;
	JSONObject json = new JSONObject();
	
	//getAccountType
	 @GetMapping("/getAccountTypes")
	 public ResponseEntity<?> getAccountTypes(){
		List<AccountType> accountTypes = accountTypeService.findAll();
		return ResponseEntity.ok().body(accountTypes);
	}
	//get List of Accounts
	 @GetMapping("/getAccounts/{username}")
	 public ResponseEntity<?> getAccounts(@PathVariable String username) {
		Customer customer = customerService.findByUsername(username);
		List<Account> accounts = accountService.findByCustomer(customer);
		
		return ResponseEntity.ok().body(accounts);
	    }
	
	//add account
	@RequestMapping(value = "/addAcc", method = RequestMethod.POST)
	public ResponseEntity<?> addAcc(@RequestBody Account account) {  
	    if(accountService.checkAccNoExists(account.getAccNo())) {
	    	json.put("code", 400);
	    	json.put("message", "Account "+account.getAccNo()+" already exists");
	    	return ResponseEntity.status(400).body(json);
	    }
	    else {
	    	String accNo = accountService.saveAccount(account);
	    	
	    	Log log = new Log();
	    	log.setOperation("Account added. Account No: " + account.getAccNo() + "; Owner: " + account.getCustomer().getFirstName()+" "+account.getCustomer().getLastName());
	        log.setTimestamp(new Timestamp(System.currentTimeMillis()));
	        log.setCustomer(account.getCustomer());
	        logService.save(log);
	        
	        json.put("code", 200);
	        json.put("message", "Account Number "+accNo + " added with FastPay.");
	    	return ResponseEntity.ok().body(json);
	    }
	}
	//delete account
	@RequestMapping(value = "/deleteAcc/{accNo}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteAcc(@PathVariable String accNo) {  
	    if(!accountService.checkAccNoExists(accNo)) {
	    	json.put("code", 400);
		    json.put("message", "Account "+accNo+" doesn't exist");
	    	return ResponseEntity.status(400).body(json);
	    }
	    else {
	    	Account account = accountService.findByAccNo(accNo);
	    	accountService.deleteAccount(accNo);
	    	
	    	Log log = new Log();
	    	log.setOperation("Account removed. Account No: " + accNo + "; Owner: " + account.getCustomer().getFirstName()+account.getCustomer().getLastName());
	        log.setTimestamp(new Timestamp(System.currentTimeMillis()));
	        log.setCustomer(account.getCustomer());
	        logService.save(log);
	        
	    	json.put("code", 200);
		    json.put("message", "Account "+accNo+" removed successfully");
	    	return ResponseEntity.ok().body(json);
	    }
	}
	
	

}
