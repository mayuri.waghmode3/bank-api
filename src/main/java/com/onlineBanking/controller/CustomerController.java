package com.onlineBanking.controller;


import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.onlineBanking.domain.Customer;
import com.onlineBanking.service.CustomerService;

@CrossOrigin("*")
@RestController
public class CustomerController {
	 @Autowired
	 private CustomerService customerService;
	 JSONObject json = new JSONObject();
		
	 @GetMapping("/login/{username}/{password}")
	 public ResponseEntity<?> login(@PathVariable String username, @PathVariable String password) {
		 Customer customer = customerService.findByUsername(username);
		 if(customer.equals(null) || customer == null)
			 return ResponseEntity.status(404).body("incorrect username or password");
		 if(customer.getUsername().equals(username) && customer.getPassword().equals(password) )
			 return ResponseEntity.ok().body(customer);
		return null;
	    
	    }
	 @RequestMapping(value = "/signup", method = RequestMethod.POST)
	 public ResponseEntity<?> signup(@RequestBody Customer newCustomer) {
	      if(customerService.checkUsernameExists(newCustomer.getUsername())) {
	    	  json.put("code", 400);
	    	  json.put("message", "Username already exists");
	    	  return ResponseEntity.ok().body(json);
	      }
	      else if(customerService.checkEmailExists(newCustomer.getEmail())) {
	    	  json.put("code", 400);
	    	  json.put("message", "Email id already exists");
	    	  return ResponseEntity.ok().body(json);
	      }
	      else {
	    	  int id = customerService.saveCustomer(newCustomer);
	    	  json.put("code", 200);
	    	  json.put("message", "Customer created with id "+id);
	    	  return ResponseEntity.ok().body(json);
	      }
	    }
	 
	 @RequestMapping(value = "/profile", method = RequestMethod.PUT)
	 public ResponseEntity<?> profile(@RequestBody Customer customer) {
		  System.out.println("Hi");
	      int id = customerService.editCustomer(customer);
	      json.put("code", 200);
    	  json.put("message", "User with username : "+customer.getUsername()+" saved");
    	  return ResponseEntity.ok().body(json);
	    }
}
