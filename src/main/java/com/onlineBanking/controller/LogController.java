package com.onlineBanking.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.onlineBanking.domain.Customer;
import com.onlineBanking.domain.Log;
import com.onlineBanking.service.CustomerService;
import com.onlineBanking.service.LogService;
@CrossOrigin("*")
@RestController
public class LogController {

	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private LogService logService;
	
	//get transaction History 
	//2019-03-26 00:00:00
	@RequestMapping(value = "/summary/{username}",method = RequestMethod.GET)
    public ResponseEntity<?> summary(@PathVariable String username) throws Exception {
        Customer customer = customerService.findByUsername(username);
        List<Log> report = logService.findByCustomer(customer);
        return ResponseEntity.ok().body(report);
    }
	
    @RequestMapping(value = "/report",method = RequestMethod.GET)
    public ResponseEntity<?> transactionsFromTo(@RequestParam String username, @RequestParam String from,@RequestParam String to) throws Exception {
        
        Customer customer = customerService.findByUsername(username);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Date fromDate = format.parse(from);
        Date toDate = format.parse(to);

        List<Log> report = logService.findByCustomerAndTimestampBetween(customer, fromDate, toDate);
       
        return ResponseEntity.ok().body(report);
    }

	
    
}
