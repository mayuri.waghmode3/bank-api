package com.onlineBanking.validators;

import com.onlineBanking.domain.Customer;

public class CustomerValidator {
	public boolean validateCustomer(Customer customer) {
        return (validateEmail(customer.getEmail()) && 
        		validateName(customer.getFirstName()) &&
        		validateName(customer.getLastName()) &&
        		validateMobile(customer.getMobileNo()) );
    }

    private boolean validateName(String name) {
        return name.matches("[a-zA-Z\\s]");
    }

    private boolean validateMobile(String mobile){
        return mobile.matches("\\d+") && (mobile.length()==10);
    }

    private boolean validateEmail(String email) {
        return email.contains("@") && email.contains(".com");
    }
}
