package com.onlineBanking.validators;

import java.math.BigDecimal;

import com.onlineBanking.domain.Account;

public class PaymentValidator {
	public boolean validate(Account from, BigDecimal amount) {
		BigDecimal subtraction = from.getBalance().subtract(amount);
        return ((subtraction.compareTo(BigDecimal.ZERO) >= 0) && amount.compareTo(BigDecimal.ZERO) > 0);
    }
	
	public boolean validate(BigDecimal amount) {
        return (amount.compareTo(BigDecimal.ZERO) > 0);
    }

}
