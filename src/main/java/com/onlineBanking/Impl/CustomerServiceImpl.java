package com.onlineBanking.Impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.onlineBanking.domain.Customer;
import com.onlineBanking.repository.CustomerRepository;
import com.onlineBanking.service.CustomerService;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;
	/*
	@Autowired
    private BCryptPasswordEncoder passwordEncoder;*/
	 
	/** The application logger */
    private static final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

	@Override
	public Customer findByUsername(String username) {
		Customer customer =  customerRepository.findByUsername(username);
		return customer;
	}
	
	
	@Override
	public Customer findByEmail(String email) {
		return customerRepository.findByEmail(email);
	}

	public boolean checkUserExists(String username){
        if (checkUsernameExists(username) || checkEmailExists(username)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean checkUsernameExists(String username) {
        if (null != findByUsername(username)) {
            return true;
        }
        return false;
    }
    
    public boolean checkEmailExists(String email) {
        if (null != findByEmail(email)) {
            return true;
        }

        return false;
    }

	@Override
	public int saveCustomer(Customer customer) {
	
	        if (this.checkUsernameExists(customer.getUsername())) {
	            log.info("Customer with userName {} exists.", customer.getUsername());
	            return customer.getId();
	        } else {
	           customer = customerRepository.save(customer);
	           log.info("Customer with userName {} created.", customer.getUsername());
	           return customer.getId();
	        }
	}
	@Override
	public int editCustomer(Customer customer) {
		
		customer = customerRepository.save(customer);
		log.info("Customer with userName {} exists. Saving profile changes ", customer.getUsername());
        return customer.getId();
	}
}
