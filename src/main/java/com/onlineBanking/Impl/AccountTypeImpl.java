package com.onlineBanking.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.onlineBanking.domain.AccountType;
import com.onlineBanking.repository.AccountTypeRepository;
import com.onlineBanking.service.AccountTypeService;
@Service
@Transactional
public class AccountTypeImpl implements AccountTypeService{
	@Autowired
	private AccountTypeRepository accountTypeRepository;
	
	@Override
	public AccountType findByTypeId(String typeId) {
		AccountType accountType = accountTypeRepository.findByTypeId(typeId);
		return accountType;
	}

	@Override
	public AccountType findByType(String type) {
		AccountType accountType = accountTypeRepository.findByType(type);
		return accountType;
	}

	@Override
	public List<AccountType> findAll() {
		List<AccountType> accountTypes = accountTypeRepository.findAll();
		return accountTypes;
	}

}
