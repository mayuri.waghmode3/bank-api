package com.onlineBanking.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.onlineBanking.domain.Account;
import com.onlineBanking.domain.Customer;
import com.onlineBanking.repository.AccountRepository;
import com.onlineBanking.service.AccountService;
@Service
@Transactional
public class AccountServiceImpl implements AccountService{
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Override
	public Account findByAccNo(String accNo) {
		Account account = accountRepository.findByAccNo(accNo);
		return account;
	}

	@Override
	public boolean checkAccNoExists(String accNo) {
		if (null != findByAccNo(accNo)) {
			return true;
	    }
		return false;
	}

	@Override
	public String saveAccount(Account account) {
		Account savedAccount = accountRepository.save(account);
		return savedAccount.getAccNo();
	}

	@Override
	public void deleteAccount(String accNo) {
		accountRepository.delete(accNo);;
	}

	@Override
	public List<Account> findByCustomer(Customer customer) {
		return accountRepository.findByCustomer(customer);
	}

	

}
