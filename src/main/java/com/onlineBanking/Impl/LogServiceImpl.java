package com.onlineBanking.Impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.onlineBanking.domain.Customer;
import com.onlineBanking.domain.Log;
import com.onlineBanking.repository.LogRepository;
import com.onlineBanking.service.LogService;

@Service
@Transactional
public class LogServiceImpl implements LogService{
	@Autowired
	private LogRepository logRepository;
	@Override
	public void save(Log log) {
		logRepository.save(log);
	}

	@Override
	public List<Log> findByCustomer(Customer customer) {
		return logRepository.findByCustomer(customer);
	}

	@Override
	public List<Log> findByCustomerAndTimestampBetween(Customer customer, Date from, Date to) {
		return logRepository.findByCustomerAndTimestampBetween(customer, from, to);
	}

}
