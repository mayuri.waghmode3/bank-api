package com.onlineBanking.service;

import org.springframework.stereotype.Service;

import com.onlineBanking.domain.Customer;

@Service
public interface CustomerService {
	Customer findByUsername(String userName);
	 
	Customer findByEmail(String email);
	
	boolean checkUserExists(String user);

    boolean checkUsernameExists(String userName);

    boolean checkEmailExists(String email);
    
    int saveCustomer (Customer customer); 
    
    public int editCustomer(Customer customer);
    
}
