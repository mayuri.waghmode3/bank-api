package com.onlineBanking.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.onlineBanking.domain.Account;
import com.onlineBanking.domain.Customer;

@Service
public interface AccountService {
	Account findByAccNo(String accNo);
	boolean checkAccNoExists(String accNo);
	String saveAccount (Account account);
	void deleteAccount(String accNo);
	List<Account> findByCustomer(Customer customer);
}
