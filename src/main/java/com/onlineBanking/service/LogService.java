package com.onlineBanking.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.onlineBanking.domain.Customer;
import com.onlineBanking.domain.Log;

@Service
public interface LogService {
	void save(Log log);
	List<Log> findByCustomer(Customer customer);

    List<Log> findByCustomerAndTimestampBetween(Customer customer,Date from, Date to);

}
