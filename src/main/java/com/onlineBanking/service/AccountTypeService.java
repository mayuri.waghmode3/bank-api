package com.onlineBanking.service;

import java.util.List;

import org.springframework.stereotype.Service;
import com.onlineBanking.domain.AccountType;

@Service
public interface AccountTypeService {
	
	AccountType findByTypeId(String typeId);
	AccountType findByType(String type);
	List<AccountType> findAll();
}
