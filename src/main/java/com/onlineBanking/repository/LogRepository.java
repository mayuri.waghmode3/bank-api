package com.onlineBanking.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.onlineBanking.domain.Customer;
import com.onlineBanking.domain.Log;



@Repository
public interface LogRepository extends JpaRepository<Log,Long> {
	List<Log> findByCustomer(Customer customer);

    List<Log> findByCustomerAndTimestampBetween(Customer customer,Date from, Date to);

}
