package com.onlineBanking.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.onlineBanking.domain.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,String>{
	Customer findByUsername(String username);
	Customer findByEmail(String email);
	
}
