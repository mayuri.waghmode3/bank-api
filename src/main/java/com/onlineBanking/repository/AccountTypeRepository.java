package com.onlineBanking.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.onlineBanking.domain.AccountType;

@Repository
public interface AccountTypeRepository extends JpaRepository<AccountType,String>{
	AccountType findByTypeId(String typeId);
	AccountType findByType(String type);
	List<AccountType> findAll();
}
