package com.onlineBanking.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.onlineBanking.domain.Account;
import com.onlineBanking.domain.Customer;

@Repository
public interface AccountRepository extends JpaRepository<Account,String>{
	Account findByAccNo(String accNo);
	List<Account> findByCustomer(Customer customer);
}
